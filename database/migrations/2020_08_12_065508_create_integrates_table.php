<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',500);
            $table->string('dni')->unique()->notNullable();
            $table->string('cargo',500)->nullable();;
            $table->text('logo');
            $table->string('slug',500)->nullable();;
            $table->date('fechaInscripcion');
            $table->integer('ppolitic_id');
            $table->string('estado',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrates');
    }
}
