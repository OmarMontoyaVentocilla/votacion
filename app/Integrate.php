<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Integrate extends Model 
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table='integrates';
    protected $fillable = [
      'nombre',
      'dni',
      'cargo',
      'logo',
      'slug',
      'fechaInscripcion',
      'ppolitic_id',
      'estado'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
}