<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\PPolitic;
use App\Integrate;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class PPoliticController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
   

    public function index(Request $request)
    {
        $response = PPolitic::where('estado',1)->get();
        return response()->json($response);
    }

    public function detalleIntegrantes($id){
           $integrantes=Integrate::where('estado',1)->where('ppolitic_id',$id)->get();
           return $integrantes; 
    }

    public function detail(Request $request){
           $pp=PPolitic::where('estado',1)->get(); 
           $arregl=[];
           foreach($pp as $partido){   
            $dt=[
               'id'=>$partido['id'],
               'logo'=>$partido['logo'],
               'slug'=>$partido['slug'],
               'nameMatchPol'=>$partido['nameMatchPol'],
               'fondoPartido'=>$partido['fondoPartido'],
               'estado'=>$partido['estado'],
              'detalleIntegrantes'=>$this->detalleIntegrantes($partido['id']), 
         ];
         array_push($arregl, $dt);
         }
         return response()->json($arregl); 
        
    }

    public function store(Request $request)
    { 
        $rules = [
            'logo' => 'required',
            'slug' => 'required',
            'nameMatchPol'=>'required',
        ];

        $this->validate($request, $rules);

        $ppolitic= new PPolitic;
        $ppolitic->logo=request('logo');
        $ppolitic->slug=request('slug');
        $ppolitic->nameMatchPol=request('nameMatchPol');
        //$ppolitic->fondoPartido=request('fondoPartido');
        request('fondoPartido')=='' ? 
        $ppolitic->fondoPartido='' :
        $ppolitic->fondoPartido=request('fondoPartido');
        $ppolitic->estado='1';

        if($ppolitic->save()){
         return response()->json([       
              "devMessage" => "success",
              "code" => 200,
              "userMessage" => "Se registro exitosamente",
          ]);
       }

    }


  public function show($id)
    {
        $ppolitic = PPolitic::findOrFail($id);
       return response()->json($ppolitic);
    }


 
    public function update(Request $request, $id)
    {
               $rules = [
                     'logo' => 'required',
                     'slug' => 'required',
                     'nameMatchPol'=>'required',
               ];

               $this->validate($request, $rules);

               $ppolitic=PPolitic::find($id);
               $ppolitic->logo=request('logo');
               $ppolitic->slug=request('slug');
               $ppolitic->nameMatchPol=request('nameMatchPol');
               request('fondoPartido')=='' ? 
               $ppolitic->fondoPartido='' :
               $ppolitic->fondoPartido=request('fondoPartido');
               //$ppolitic->fondoPartido=request('fondoPartido');
               $ppolitic->estado='1';
               if($ppolitic->save()){
                  return response()->json([       
                     "devMessage" => "success",
                     "code" => 200,
                     "userMessage" => "Se actualizo exitosamente",
                  ]);
               }
    }

    public function destroy($id)
    {
      $ppolitic=PPolitic::find($id);
      $ppolitic->estado = 0;
      if ($ppolitic->save()) {
         return response()->json([
            "devMessage" => "success",
            "code" => 200,
            "userMessage" => "Se elimino exitosamente",
         ]);
     }

    }

}