<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Integrate;
use App\Voto;
use App\Particpant;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class VotoController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */

   public function detalleDni($dni){
      $responseIntegrante = Integrate::where('dni',$dni)->where('estado',1)->count();
      $responseParticipante = Particpant::where('dni',$dni)->where('estado',1)->count();
      $response='';
      if($responseIntegrante==0 && $responseParticipante==0){  
         $response =[];
      }else if($responseIntegrante==0 && $responseParticipante!=0){
        $data = Particpant::where('dni',$dni)->where('estado',1)->get();
        $response =$data;
     }else if($responseIntegrante!=0 && $responseParticipante==0){
        $data = Integrate::where('dni',$dni)->where('estado',1)->get();
        $response =$data;
     }else{
       $response =[];
     } 
     return $response;
   }
    
    public function index(Request $request)
    {

      $votos=Voto::join('ppolitic', 'voto.id_partido', '=', 'ppolitic.id')
      ->select('voto.id as idVoto',
               'voto.id_partido as idPartido',
               'voto.dni',
               'voto.fechaVoto',
               'ppolitic.slug as descripPartido',
               'ppolitic.nameMatchPol as nombrePartido'
      )->where('voto.estado',1)->get();

      $arregloVoto=[];
         foreach($votos as $voto){   
            $dt=[
               'idVoto'=>$voto['idVoto'],
               'idPartido'=>$voto['idPartido'],
               'dni'=>$voto['dni'],
               'fechaVoto'=>$voto['fechaVoto'],
               'descripPartido'=>$voto['descripPartido'],
               'nombrePartido'=>$voto['nombrePartido'],
               'detalleDni'=>$this->detalleDni($voto['dni']),
            
         ];
         array_push($arregloVoto, $dt);
         }
         return response()->json($arregloVoto); 
      }


      public function show(Request $request,$id)
      {
  
        $votos=Voto::join('ppolitic', 'voto.id_partido', '=', 'ppolitic.id')
        ->select('voto.id as idVoto',
                 'voto.id_partido as idPartido',
                 'voto.dni',
                 'voto.fechaVoto',
                 'ppolitic.slug as descripPartido',
                 'ppolitic.nameMatchPol as nombrePartido'
        )->where('voto.estado',1)->where('voto.id',$id)->get();
  
        $arregloVoto=[];
           foreach($votos as $voto){   
              $dt=[
                 'idVoto'=>$voto['idVoto'],
                 'idPartido'=>$voto['idPartido'],
                 'dni'=>$voto['dni'],
                 'fechaVoto'=>$voto['fechaVoto'],
                 'descripPartido'=>$voto['descripPartido'],
                 'nombrePartido'=>$voto['nombrePartido'],
                 'detalleDni'=>$this->detalleDni($voto['dni']),
              
           ];
           array_push($arregloVoto, $dt);
           }
           return response()->json($arregloVoto); 
        }
    
    

    public function postVoto(Request $request)
    {
         $dni=$request->dni;
         $idPartido=$request->id_partido;  
         $fecha= date("Y-m-d"); 
       
         $response='';
         $responseIntegrante = Integrate::where('dni',$dni)->where('estado',1)->count();
         $responseParticipante = Particpant::where('dni',$dni)->where('estado',1)->count();

        if($responseIntegrante==0 && $responseParticipante==0){
            
            $response =[
               'data' => [
                  "devMessage" => "error",
                  "code" => 404,
                  "data"=>[],
                  "type"=>false,
                  "userMessage" => "No existe el DNI en nuestro registro",
               ]
            ];
            return response()->json($response);

        }else if($responseIntegrante==0 && $responseParticipante!=0){
           
                  $checkVoto = Voto::where('dni',$dni)->where('estado',1)->count();
                   if($checkVoto==0){
                     $voto = new Voto;
                     $voto->dni=request('dni');
                     $voto->id_partido=request('id_partido');
                     $voto->fechaVoto=$fecha;
                     $voto->estado='1';
   
                     if($voto->save()){
   
                        $response =[
                           'data' => [
                              "devMessage" => "success",
                              "code" => 200,
                              "data"=>[],
                              "type"=>true,
                              "userMessage" => "Se registro exitosamente"
                           ]
                        ];
                        return response()->json($response);
                     }
                  }else{
                     $response =[
                        'data' => [
                           "devMessage" => "success",
                           "code" => 200,
                           "data"=>[],
                           "type"=>true,
                           "userMessage" => "El DNI existente ya ha realizado la votación"
                        ]
                     ];
                     return response()->json($response);
                  }
                 

        }else if($responseIntegrante!=0 && $responseParticipante==0){
         $checkVoto = Voto::where('dni',$dni)->where('estado',1)->count();
         if($checkVoto==0){
           $voto = new Voto;
           $voto->dni=request('dni');
           $voto->id_partido=request('id_partido');
           $voto->fechaVoto=$fecha;
           $voto->estado='1';

           if($voto->save()){

              $response =[
                 'data' => [
                    "devMessage" => "success",
                    "code" => 200,
                    "data"=>[],
                    "type"=>true,
                    "userMessage" => "Se registro exitosamente"
                 ]
              ];
              return response()->json($response);
           }
        }else{
           $response =[
              'data' => [
                 "devMessage" => "success",
                 "code" => 200,
                 "data"=>[],
                 "type"=>true,
                 "userMessage" => "El DNI existente ya ha realizado la votación"
              ]
           ];
           return response()->json($response);
        }
        }else{
         $response =[
            'data' => [
               "devMessage" => "error",
               "code" => 500,
               "data"=>[],
               "type"=>false,
               "userMessage" => "Error",
            ]
         ];
         return response()->json($response);
        } 
        
    }

}


