<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Integrate;
use App\Particpant;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class ParticpantController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
   

    public function index(Request $request)
    {
        $response = Particpant::where('estado',1)->get();
        return response()->json($response);
    }

    public function store(Request $request)
    { 
        $rules = [
            'nombre' => 'required',
            'dni'=>'required',
            'empresaPart'=>'required'
        ];

        $this->validate($request, $rules);

        $resultadoIntegrante=Integrate::where('dni', request('dni'))->count();
        $resultadoParticipante=Particpant::where('dni', request('dni'))->count();
         if($resultadoIntegrante==0 && $resultadoParticipante==0){
            $ppolitic= new Particpant;
            $ppolitic->nombre=request('nombre');
            $ppolitic->dni=request('dni');
            $ppolitic->slug='PARTICIPANTE';
            $ppolitic->empresaPart=request('empresaPart');
            $ppolitic->estado='1';
    
            if($ppolitic->save()){
             return response()->json([       
                  "devMessage" => "success",
                  "code" => 200,
                  "userMessage" => "Se registro exitosamente",
              ]);
           }
         }else{
            return response()->json([       
               "devMessage" => "error",
               "code" => 403,
               "userMessage" => "Ya se registro anteriormente",
           ]);  
         }  

       

    }


  public function show($id)
    {
        $ppolitic = Particpant::findOrFail($id);
       return response()->json($ppolitic);
    }


 
    public function update(Request $request, $id)
    {
      $rules = [
         'nombre' => 'required',
         'dni'=>'required',
     ];
               $this->validate($request, $rules);

               $ppolitic=Particpant::find($id);
               $ppolitic->nombre=request('nombre');
               $ppolitic->dni=request('dni');
               $ppolitic->slug='PARTICIPANTE';
               $ppolitic->empresaPart=request('empresaPart');
               $ppolitic->estado='1';
               if($ppolitic->save()){
                  return response()->json([       
                     "devMessage" => "success",
                     "code" => 200,
                     "userMessage" => "Se actualizo exitosamente",
                  ]);
               }
    }

    public function destroy($id)
    {
      $ppolitic=Particpant::find($id);
      $ppolitic->estado = 0;
      if ($ppolitic->save()) {
         return response()->json([
            "devMessage" => "success",
            "code" => 200,
            "userMessage" => "Se elimino exitosamente",
         ]);
     }

    }

}


