<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Integrate;
use App\Particpant;
use App\Traits\ApiResponse;
use Illuminate\Http\Response;

class DniController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
   

    public function index(Request $request, $dni)
    {
         $response='';
         $responseIntegrante = Integrate::where('dni',$dni)->where('estado',1)->count();
         $responseParticipante = Particpant::where('dni',$dni)->where('estado',1)->count();

        if($responseIntegrante==0 && $responseParticipante==0){
            
            $response =[
               'data' => [
                  "devMessage" => "error",
                  "code" => 404,
                  "data"=>[],
                  "type"=>false,
                  "userMessage" => "No existe ningún registro",
               ]
            ];

        }else if($responseIntegrante==0 && $responseParticipante!=0){
           $data = Particpant::where('dni',$dni)->where('estado',1)->get();
           $response =[
            'data' => [
               "devMessage" => "success",
               "code" => 200,
               "data"=>$data,
               "type"=>true,
               "userMessage" => "existe",
            ]
         ];


        }else if($responseIntegrante!=0 && $responseParticipante==0){
           $data = Integrate::where('dni',$dni)->where('estado',1)->get();
           $response =[
            'data' => [
               "devMessage" => "success",
               "code" => 200,
               "data"=>$data,
               "type"=>true,
               "userMessage" => "existe",
            ]
         ];
        }else{
         $response =[
            'data' => [
               "devMessage" => "error",
               "code" => 500,
               "data"=>[],
               "type"=>false,
               "userMessage" => "Error",
            ]
         ];
        } 
        return response()->json($response);
    }

}


