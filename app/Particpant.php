<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Particpant extends Model 
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table='participants';
    protected $fillable = [
      'nombre',
      'dni',
      'empresaPart',
      'slug',
      'estado'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
}