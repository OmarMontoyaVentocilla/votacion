<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Voto extends Model 
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table='voto';
    protected $fillable = [
      'id_partido',
      'dni',
      'fechaVoto',
      'estado'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
}