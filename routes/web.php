<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {

    return "401 token not send";
});


$router->group(['middleware'=>'auth'], function () use  ($router){

    $router->get('/test', 'AuthController@test');  
    
    $router->group(['prefix' => 'partido-politico'], function () use ($router) {

        $router->get('/', 'PPoliticController@index');
        $router->post('/', 'PPoliticController@store');
        $router->get('/{id}', 'PPoliticController@show');
        $router->put('/{id}', 'PPoliticController@update');
        $router->delete('/{id}', 'PPoliticController@destroy');
    
    });

    $router->group(['prefix' => 'integrante'], function () use ($router) {

        $router->get('/', 'IntegrateController@index');
        $router->post('/', 'IntegrateController@store');
        $router->get('/{id}', 'IntegrateController@show');
        $router->put('/{id}', 'IntegrateController@update');
        $router->delete('/{id}', 'IntegrateController@destroy');
    });

    $router->group(['prefix' => 'participante'], function () use ($router) {

        $router->get('/', 'ParticpantController@index');
        $router->post('/', 'ParticpantController@store');
        $router->get('/{id}', 'ParticpantController@show');
        $router->put('/{id}', 'ParticpantController@update');
        $router->delete('/{id}', 'ParticpantController@destroy');
    
    });

});

$router->get('/partido-politicos/integrante', 'PPoliticController@detail');
$router->get('/search-dni/{dni}', 'DniController@index');
$router->post('/voto', 'VotoController@postVoto');
$router->get('/voto', 'VotoController@index');
$router->get('/voto/{id}', 'VotoController@show');
$router->post('/login', 'AuthController@login');
$router->post('/register', 'AuthController@store');
// $router->post('/logout', 'AuthController@logout');
// $router->post('/refresh', 'AuthController@refresh');


